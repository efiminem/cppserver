#define LOGOUTPATH "/var/www/cppserver/public/log.txt"
#define DOCTYPEPATH "/var/www/cppserver/private/doctype.tmpl"
#define HEADPATH "/var/www/cppserver/private/head.tmpl"
#define BODYPATH "/var/www/cppserver/private/body.tmpl"

#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string.h>

#include "fcgi_config.h"
#include "fcgiapp.h"


#define THREAD_COUNT 8
#define SOCKET_PATH "/tmp/cppserver/local.sock"

int counter=0;
std::ofstream logout;
std::string postbuff_str="";
std::string getbuff_str="";
std::ifstream htmlstream;
std::string htmlstring;

static int socketId;

static void *doit(void *a)
{
    int rc, i;
    FCGX_Request request;
    char *get_name;
    char *get_addr;

    if(FCGX_InitRequest(&request, socketId, 0) != 0)
    {
        return NULL;
    }

    for(;;)
    {
        static pthread_mutex_t accept_mutex = PTHREAD_MUTEX_INITIALIZER;
        counter++;
        char counter_buf[10];
        sprintf(counter_buf,"%d",counter);
        pthread_mutex_lock(&accept_mutex);
        rc = FCGX_Accept_r(&request);
        pthread_mutex_unlock(&accept_mutex);

        if(rc < 0)
        {
            break;
        }

        get_name = FCGX_GetParam("QUERY_STRING", request.envp);
        get_addr = FCGX_GetParam("REMOTE_ADDR", request.envp);
        logout.open(LOGOUTPATH, std::fstream::out | std::fstream::app);
        logout << "IP: " << get_addr <<'\n';
        logout.close();
        if (get_name[0] !=0) {
        logout.open(LOGOUTPATH, std::fstream::out | std::fstream::app);
        logout << "Get: " << get_name << '\n';
        logout.close();
        getbuff_str=get_name;
        }
        char postbuff[1000]={0};
        FCGX_GetStr(postbuff, sizeof(postbuff), request.in);
        if (postbuff[0]!=0) {
        logout.open(LOGOUTPATH, std::fstream::out | std::fstream::app);
        logout << "Post: " << postbuff << '\n';
        postbuff_str=postbuff;
        logout.close();
        }
        if (strcmp(get_name,"mitkovets")) {
        FCGX_PutS("Content-type: text/html\r\n", request.out);
        FCGX_PutS("\r\n", request.out);
        htmlstream.open(DOCTYPEPATH);
        while(std::getline(htmlstream, htmlstring)) {
        FCGX_PutS(htmlstring.c_str(), request.out);
        FCGX_PutS("\r\n", request.out); //only for readability
        }
        htmlstream.close();
        FCGX_PutS("<html xmlns=\"http:/", request.out);
        FCGX_PutS("/www.w3.org/1999/xhtml\" lang=\"ru\">\r\n", request.out);
        FCGX_PutS("<head>\r\n", request.out);
        htmlstream.open(HEADPATH);
        while(std::getline(htmlstream, htmlstring)) {
        FCGX_PutS(htmlstring.c_str(), request.out);
        FCGX_PutS("\r\n", request.out); //only for readability
        }
        htmlstream.close();
        FCGX_PutS("</head>\r\n", request.out);
        FCGX_PutS("<body>\r\n", request.out);
        htmlstream.open(BODYPATH);
        while(std::getline(htmlstream, htmlstring)) {
        FCGX_PutS(htmlstring.c_str(), request.out);
        FCGX_PutS("\r\n", request.out); //only for readability
        }
        htmlstream.close();
        FCGX_PutS("</body>\r\n", request.out);
        FCGX_PutS("</html>", request.out);
        }
        else {
        FCGX_PutS("Content-type: text/plain\r\n", request.out);
        FCGX_PutS("\r\n", request.out);
        FCGX_PutS("Hello Mr Mitkovets!", request.out);

        }

        FCGX_Finish_r(&request);

    }

    return NULL;
}

int main(void)
{

    logout.open(LOGOUTPATH, std::fstream::out); //rewrite
    logout.close();
    pid_t parpid, sid;

    parpid = fork();
    if(parpid < 0) {
        exit(1);
    } else if(parpid != 0) {
        exit(0);
    }
    sid = setsid();
    if(sid < 0) {
        exit(1);
    }
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    int i;
    pthread_t id[THREAD_COUNT];

    FCGX_Init();
    printf("Lib is inited\n");

    socketId = FCGX_OpenSocket(SOCKET_PATH, 20);
    if(socketId < 0)
    {
        return 1;
    }
    printf("Socket is opened\n");

    for(i = 0; i < THREAD_COUNT; i++)
    {
        pthread_create(&id[i], NULL, doit, NULL);
    }

    for(i = 0; i < THREAD_COUNT; i++)
    {
        pthread_join(id[i], NULL);
    }

    logout.close();
    return 0;
}
